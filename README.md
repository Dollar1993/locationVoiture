# LocationVoiture

Projet de Location de Voiture Pour L'entreprise **SamaOtOBi**
# Fonctionalités
- ### Gerant 
  * CRUD Voiture (Cheikh Ibra SAMB)
  * CRUD offre   (Cheikh Ibra SAMB)
  * SUIVRE RESERVATION (Cheikh Ibra SAMB)
  * SUIVRE LOCATION  (Cheikh Ibra SAMB)
  * GESTION USER     (Alioune Bada NDOYE)
- ### Visiteurs
  * Consulter les offres (Mamadou Sedyou DIALLO)
  * S'inscrire sur le site (Mamadou Sedyou DIALLO)
- ### Clients
  * Consulter offre     (Mamadou Sedyou DIALLO)
  * Gérer favoris        (Mamadou Sedyou DIALLO)
  * Reserver des voitures  (Mamadou Sedyou DIALLO)
 * Suivre Reservation   (Mamadou Sedyou DIALLO)

#### NB :
- Assurer la sécurité d'accès à certaines fonctionalités par une authentification dynamique.  (Alioune Bada NDOYE)
- Contrôle de la saisie dans les formulaires  (Alioune Bada NDOYE)
Nous aurons trois pages principales dans l'application.

- Page Admin 
- Page Visiteur
- Page Client
- Page Authentification
- Page Inscription

### Diagramme de classes
![Diagramme de classes](location_modele.png)

