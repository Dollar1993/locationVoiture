--
-- Base de données : `samaOtOBi`
--
drop   database if exists `samaoto`;
create database if  not exists  `samaoto`;
use samaoto;
-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF  NOT EXISTS `utilisateur` (
  `id` int(11)  NOT NULL AUTO_INCREMENT,
  ` m` varchar(45) DEFAULT NULL,
  `pre m` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `adresse` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `motDePasse` varchar(45) DEFAULT NULL,
  `sexe` varchar(45) DEFAULT NULL,
  `profil` varchar(45) DEFAULT NULL,
  `etat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Structure de la table `offre`
--

DROP TABLE IF EXISTS `offre`;
CREATE TABLE IF NOT EXISTS `offre` (
  `id` int(11)  NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `dateDebut` DATE DEFAULT NULL,
  `dateFin` DATE DEFAULT NULL,
  `montant`  varchar(100) DEFAULT NULL,
  `remise` int(11) DEFAULT NULL,
  `voitureId_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voitureId_idx` (`voitureId_idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `etat` varchar(100) DEFAULT NULL,
  `dateReservation` DATE DEFAULT NULL,
  `utilisateurId_idx` int(11) DEFAULT NULL,
  `voitureId_idy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `utilisateurId_idx` (`utilisateurId_idx`),
  KEY `voitureId_idy` (`voitureId_idy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `voiture`;
CREATE TABLE IF NOT EXISTS `voiture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `immatriculation` varchar(100) DEFAULT NULL,
  `dateAchat` DATE DEFAULT NULL,
  `kmParcouru` int(11) DEFAULT NULL,
  ` mbrePlace` int(11) DEFAULT NULL,
  `categorie` varchar(100) DEFAULT NULL,
  `marque` varchar(100) DEFAULT NULL,
  `puissance` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `kmDepart` int(11) DEFAULT NULL,
  `kmRetour` int(11) DEFAULT NULL,
  `dateDebut` DATE DEFAULT NULL,
  `montant` varchar(100) DEFAULT NULL,
  `remise` varchar(11) DEFAULT NULL,
  `dateFin` DATE DEFAULT NULL,
  `voitureId_idz` int(11) DEFAULT NULL,
  `reservationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voitureId_idz` (`voitureId_idz`),
  KEY `reservationId_idx` (`reservationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ressource`;
CREATE TABLE IF NOT EXISTS `ressource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  ` m` varchar(11) DEFAULT NULL,
  `voitureId_ida` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voitureId_ida` (`voitureId_ida`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `favoris`;
CREATE TABLE IF NOT EXISTS `favoris` (
  `id` int(11)  NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) DEFAULT NULL,
  `utilisateurId_idb` int(11) DEFAULT NULL,
  `offre_idb` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `utilisateurId_idb` (`utilisateurId_idb`),
  KEY `offre_idb` (`offre_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `utilisateurId_idx` FOREIGN KEY (`utilisateurId_idx`) REFERENCES `utilisateur` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE,
  ADD CONSTRAINT `voitureId_idy` FOREIGN KEY (`voitureId_idy`) REFERENCES `voiture` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE;
ALTER TABLE `offre`
ADD CONSTRAINT `voitureId_idx` FOREIGN KEY (`voitureId_idx`) REFERENCES `voiture` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE;
ALTER TABLE `location`
  ADD CONSTRAINT `voitureId_idz` FOREIGN KEY (`voitureId_idz`) REFERENCES `voiture` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE,
  ADD CONSTRAINT `reservationId` FOREIGN KEY (`reservationId`) REFERENCES `reservation` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE;
ALTER TABLE `favoris`
  ADD CONSTRAINT `utilisateurId_idb` FOREIGN KEY (`utilisateurId_idb`) REFERENCES `utilisateur` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE,
  ADD CONSTRAINT `offre_idb` FOREIGN KEY (`offre_idb`) REFERENCES `offre` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE;
ALTER TABLE `ressource`
  ADD CONSTRAINT `voitureId_ida` FOREIGN KEY (`voitureId_ida`) REFERENCES `voiture` (`id`) ON DELETE   CASCADE ON UPDATE   CASCADE;
