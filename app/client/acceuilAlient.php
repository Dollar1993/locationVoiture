<?php
    session_start();
    require_once("./connexion/dbconnexion.php"); 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>LOCATION ACCUEIL</title>
        <link rel="stylesheet" type="text/css" href="styleaccueil.css">
       
    </head>
    <body >
        <!-- Hiello je suis l'entete de la page c'est moi la belle voiture qui contient les options -->
        <div id="entete"> <!--debut entete-->
            <img class="image_entete" src="image/fondindex.JPG" /> 
            <div id="trucs">
                <p class="nomsite"> Kangam Automobile</p> 
              
                <a href="index.php?op=1"><b id="lesoptions" class="options">Offres</b></a> 
                <a href="index.php?op=2"><b class="options">Espace Clients</b></a>
                <a href="index.php?op=3"><b class="options">S'incrire</b></a>
                <a href="index.php?op=4"><b class="options">Se connecter</b></a>
                <a href="index.php?op=5"><b class="options">FAQ</b></a>
                                           
            </div>          
            <div id="formrecherche">
                <form name="formrecherche" method="post" action="">
                    <input id="motcle" type="text" name="motcle" placeholder="Recherche par Marque...." />
                    <input class="btnsubmit" type="submit" name="btnsumit" value="Recherche" />
                    
                </form>
                
            </div>
        </div> <!--fin Entete-->
        <!-- je suis la partie blanche qui est juste apres l'entete je change de contenu en fonction de l'option cliqué  -->
        <div id="apropos"><!--la partie gauche-->
            <p id="propos">A propos de ...</p>
            <p class="kangam">Bienvenue sur Kangam Automobile </p>
            <p class="kangam">Notre site de location en ligne</p>
            <p class="kangam">De très bon véhicules à très bon prix !</p>
            <!-- <p class="kangam">Réservez 7j/7 24h/24</p> -->
            <p  class="letext"><b>Kangam Automobile</b> est la solution idéale vous permettant de louer  une voiture sur de longues périodes tout en maîtrisant votre budget. C'est un acteur incontournable dans la location de voitures au Sénégal. Louer chez Kangam Automobile, c’est non seulement faire le bon choix , mais aussi la promesse :
            <p class="fonc">° D’un parc automobile de dernière génération<br>° D’un large choix de véhicules<br>° D’une assistance 7j/7 24h/24<br>° De formules adaptées à vos besoins</p>
            <p class="kangamfin">Plus qu’un simple service, Kangam Automobile est <b>VOTRE</b> partenaire pour une location fiable.Les meilleures décisions de voyage se prennent avec <b>KANGAM AUTOMOBILE</b></p>
        </div><!--fin partie gauche-->
        <div id="blancmilieu"><!--debut milieu-->
            <?php 
                if (isset($_GET['op'])) {
                    $l=$_GET['op'];
                    if ($l==3) {
                        # je méne vers la page d'inscription
                    ?>
                    <div id="ins"> <!-- div ins -->
                      <div id="contenu"> <!-- div contenu -->
                      <p id="erreur">
                       <?php
                        if(isset($_GET['erreur'])){
                            $err=$_GET['erreur'];
                            if($err==1 || $err==3 || $err==2 || $err==4 || $err=5){
                                echo "Une erreur est survenue lors de l'inscription , veuillez réessayer ultérieurement ! ";
                            }
                            else
                                echo'';
                        }
                       ?>
                      </p>
                       <form id="inscription" name="inscription" action="traiterInscription.php" method="POST">
                       <h1>Inscription</h1>
                       <label><b>Nom</b></label>
                       <input type="text" placeholder="Entrer votre nom" name="nom" required>
                       <label><b>Prénom</b></label>
                       <input type="text" placeholder="Entrer votre prénom" name="prenom" required>
                       <label><b>Téléphone</b></label>
                       <input type="tel" placeholder="Entrer votre numéro de telephone" name="telephone" required>
                       <label><b>E-mail</b></label>
                       <input type="email" placeholder="Entrer votre email" name="mail" pattern="[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z.]{2,4}$" required>                
                       <label><b>Adresse</b></label>
                       <input type="text" placeholder="Où habitez-vous?" name="adresse" required>
                       <label><b>Nom d'utilisateur</b></label>
                       <input type="text" placeholder="Entrer un nom d'utilisateur" name="login" id="login" required>
                       <!-- champs pour afficher ce login existe deja -->
                       <p style="color:red;" id="loginjs">
                       <?php
                        if(isset($_GET['erreur'])){
                            $err=$_GET['erreur'];
                            if($err==1){
                                echo "ce login est deja utilisé veuillez choisir un autre";
                            }
                            else
                                echo'';
                        }
                        ?>
                        </p>
                        <label><b>Mot de passe</b></label>
                        <input type="password" placeholder="Entrer le mot de passe" name="mdp1" id="mdp1" required>
                        <p id="pwdjs" style="color: red;"></p>
                        <label><b>Confirmer le mot de passe</b></label>
                        <input type="password" placeholder="Entrer le mot de passe" name="mdp2" id="mdp2" required>
                        <p>
                        <p id="mdpjs" style="color: red;"></p>
                        <label><b> Sexe : </b></label>
                        <span class="check">M</span>
                        <input type="radio" name="sexe" value="M"/>
                        <span class="check">F</span>
                        <input type="radio" name="sexe" value="F"/>
                        </p>
                        <input type="submit" id='submit' value="S'incrire" >
                        </form>
                            <p style="color:red;" id="erreur"></p>
                        </div> <!-- fin div contenu -->
                        <!-- <p style="color:red;" id="erreur"></p> -->
                        <script type="text/javascript" src="./page.js"></script>  
                        <div id="droiteins">
                            <div id="divcnx">
                            <div id="contenucnx">
                            <p id="instest">
                            <?php
                            if(isset($_GET['instest'])){
                                $ok=$_GET['instest'];
                                if($ok==1 || $ok==2){
                                echo "inscription réussie ! vous pouvez desormais vous connectez ";
                            }
                            else
                                echo'';
                            }
                            ?>
                            </p>

                            <form id="formcnx" action="traiterConnexion.php" method="POST">
                            <h1>Connexion</h1>
                
                            <label><b>Nom d'utilisateur</b></label>
                            <input id="text" type="text" placeholder="Entrer le nom d'utilisateur" name="login" required>

                            <label><b>Mot de passe</b></label>
                            <input id="pwd" type="password" placeholder="Entrer le mot de passe" name="mdp" required>

                            <input type="submit" id='submit' value='LOGIN' >
                            <?php
                            if(isset($_GET['erreur'])){
                            $err=$_GET['erreur'];
                            if($err==1 || $err==0 || $err=2 || $err=3 || $err=4 ){
                                ?>
                                <p style="color:red; background-color: white; padding: 4px;">
                                  Nom d'utilisateur ou mot de passe incorrect veuilez réessayer encore !
                                 </p>  
                                <?php
                            }
                            else
                                echo'';
                            }
                            ?>                
                            </form>
                            </div>
                           </div>    
                            <!--  ?> -->
                        </div> <!-- fin div droiteins -->          
                        </div> <!-- fin div ins -->

                    <?php    
                }
                }
             ?>
            
        </div><!--fin milieu-->
    </body>
</html>